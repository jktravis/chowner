# Purpose
This has a highly specialized purpose. To recursively update the owner meta tag
for the given directory and file extensions.

# Requirements
* [Apache Commons IO][1]
The appropirate file will be downloaded automatically by the Ant build script,
if you decide to use it. If so, just use `ant init` to initialize.
* [ Java 1.6+ JRE ][2]

# Usage
Simply enter the text you'd like to use for the replacement in the appropriate
field, set the path to begin searching, desginate the file extensions to search
using a comma separated list, click "Presto-Chango" and you're good!

[1]: http://commons.apache.org
[2]: http://java.com/
