package com.jktravis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashSet;

public class MainGui implements Runnable
{
    JTextArea output = new JTextArea(13, 38);
    JTextField newText = new JTextField();
    JTextField rootDir = new JTextField("/Users/jtravis/Scratch/foo");
    JTextField includeExt = new JTextField("asp,html");
    JButton button = new JButton("Presto-Chango!");
    HashSet<String> extensions;
    ChgOwner chgOwner;

    public void loadGui()
    {
        JFrame frame = new JFrame("Change Owner");

        JPanel panel = new JPanel();
        JPanel topPanel = new JPanel();
        JPanel secondPanel = new JPanel();
        JPanel thirdPanel = new JPanel();
        JPanel fourthPanel = new JPanel();
        JPanel fifthPanel = new JPanel();
        JPanel lastPanel = new JPanel();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        Dimension textFieldDim = new Dimension(350, 30);
        Dimension labelDim = new Dimension(120, 30);
        Dimension extDim = new Dimension(120, 60);

        JLabel newTextLabel = new JLabel("New Text", SwingConstants.RIGHT);
        newTextLabel.setPreferredSize(labelDim);
        newText.setPreferredSize(textFieldDim);
        newText.addActionListener(new startListener());

        JLabel rootDirLabel = new JLabel("Starting Directory", SwingConstants.RIGHT);
        rootDirLabel.setPreferredSize(labelDim);
        rootDir.setPreferredSize(textFieldDim);
        rootDir.addActionListener(new startListener());

        JLabel includeExtLabel = new JLabel("<html>File Extensions<br/>(Comma Separated)</html", SwingConstants.RIGHT);
        includeExtLabel.setPreferredSize(extDim);
        includeExt.setPreferredSize(textFieldDim);
        includeExt.addActionListener(new startListener());

        JLabel outputLabel = new JLabel("Output", SwingConstants.LEFT);
        output.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(output);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        topPanel.add(newTextLabel);
        topPanel.add(newText);

        thirdPanel.add(rootDirLabel);
        thirdPanel.add(rootDir);

        fourthPanel.add(includeExtLabel);
        fourthPanel.add(includeExt);

        fifthPanel.add(outputLabel);
        fifthPanel.add(scrollPane);

        lastPanel.add(button);

        panel.add(topPanel);
        panel.add(secondPanel);
        panel.add(thirdPanel);
        panel.add(fourthPanel);
        panel.add(fifthPanel);
        panel.add(lastPanel);

        frame.getContentPane().add(BorderLayout.NORTH, panel);

        button.addActionListener(new startListener());

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setSize(500, 500);
        frame.setVisible(true);
    }

    @Override
    public void run()
    {
        this.loadGui();
    }

    public void parseExt()
    {
        extensions = new HashSet<String>();

        String[] exts = includeExt.getText().split(",");

        for (String ext : exts)
        {
            ext = ext.trim();
            extensions.add(ext);
        }
    }

    public boolean validateInput()
    {

        if (newText.getText().length() < 3)
            return false;
        if (rootDir.getText().equals(""))
            return false;
        if (includeExt.getText().equals(""))
            return false;
        return true;
    }

    class startListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {

            output.setText("");

            if (validateInput())
            {
                output.append("Starting...\n\n");
                parseExt();
                chgOwner = new ChgOwner(new File(rootDir.getText()), output, newText.getText(), extensions);
                Thread threadTwo = new Thread(chgOwner);
                threadTwo.start();
            }
            else
            {
                output.append("Parameter missing.");
            }
        }
    }
}
