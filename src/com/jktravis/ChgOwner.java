package com.jktravis;

import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import java.io.*;
import java.util.HashSet;

public class ChgOwner implements Runnable
{
    String regex = "(<meta name=\"owner\" content=\").[^\"]*(.*>)";
    File file;
    HashSet<String> extensions;
    String newText;
    JTextArea output;

    public ChgOwner(File file, JTextArea output, String newText, HashSet<String> ext)
    {
        this.newText = newText;
        this.extensions = ext;
        this.output = output;
        this.file = file;
    }
    public void walkin(File dir)
    {
        String ext;

        File listFile[] = dir.listFiles();

        if (listFile != null)
        {
            for (File aListFile : listFile)
            {
                if (aListFile.isDirectory())
                {
                    walkin(aListFile);
                } else
                {
                    ext = FilenameUtils.getExtension(aListFile.getName());
                    if (extensions.contains(ext))
                    {
                        output.append(aListFile.getPath() + "\n");
                        replace(aListFile.getPath());
                    }
                }
            }
        }
    }

    public void replace(String fileName)
    {
        String tmpFileName = fileName + ".tmp";

        BufferedReader br = null;
        BufferedWriter bw = null;

        try
        {
            br = new BufferedReader(new FileReader(fileName));
            bw = new BufferedWriter(new FileWriter(tmpFileName));
            String line;
            while ((line = br.readLine()) != null)
            {
                line = line.replaceAll(regex, "$1" + newText +  "$2");
                bw.write(line+"\n");
            }
        }
        catch (Exception e)
        {
            return;
        }
        finally
        {
            try
            {
                if(br != null)
                    br.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            try
            {
                if(bw != null)
                    bw.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        // Once everything is complete, delete old file..
        File oldFile = new File(fileName);
        oldFile.delete();

        // And rename tmp file's name to old file name
        File newFile = new File(tmpFileName);
        newFile.renameTo(oldFile);
    }


    @Override
    public void run()
    {
        walkin(file);

        extensions.clear();
        output.append("\nCompleted.");
    }
}
